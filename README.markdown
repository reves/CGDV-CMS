# Content Management System for Con Ganas de Vivir

[Con Ganas de Vivir](http://www.cgdv.org) is a non-profit organization that supports Oncological patients, by [Fernando D. Ramirez F.]

##### Version 1.4.9
	Rails 3.2.22.5

##### Version 1.4.8
	- Rails 3.2.17

##### Version 1.4.7
	- Rails 3.2.13
	- Creación de usuarios por administradores
	- Asignación de Roles.

##### Version 1.4.6
	- Migrated to Ruby 1.9.3

##### Version 1.4.5
	- Removed Logo

##### Version 1.4.4
	- File attachments for Patients and Notes

##### Version 1.4.3
	- Rails 3.2.2
	
##### Version 1.4.2
	- Private file attachments with Carrierwave.
	- Rails 3.1.3

##### Version 1.4.1
	- Added Carrierwave gem.

##### Version 1.4.0
	- Rails Pipeline integrated.
	- Moved from heroku to passenger shared hosting.

##### Version 1.3.0
	- Rails 3.1.1
	- Removed Gravatar

##### Version 1.2.9
	- Migrated to Rails 3.1, not ussing Asset Pipeline yet.
	- Corrected nested_form gem for Rails 3.1.

##### Version 1.2.8
	- Updated MetaSearch to Ransack gem

##### Version 1.2.7
	- Polymorphic Controller for Polymorphic models.
	- Donaciones.
	
##### Version 1.2.6
	- Social Service Reports.
	- Users lift.
	- Check user hours.
	
##### Version 1.2.5
 	- Updated Time management in Volunteers
 	- Added JQuery plugin jQuery Textarea Characters Counter Plugin v 2.0 for Word/Character count in TextArea fields.

##### Version 1.2.4
 	- List of events in Timereports
 	- Name appears in new Timereport
 	- Accumulated hours in Timereports index
 	- Added more Subprograms to Volunteers

##### Version 1.2.3
 	- Autocomplete Fields in Addresses

##### Version 1.2.2
 	- International Addresses
 	
##### Version 1.2.1
 	- Minor Changes

##### Version 1.2
 	- Donors
 
##### Version 1.1
 	- Defunciones
 	- Roles
 	
##### Version 1.0
 	- Devise
 	- Arranque en Github

# LICENSE

### Copyright (c) 2011 copyright [Con Ganas de Vivir](http://www.conganas.org.mx), [Fernando D. Ramirez F.](http://revomx.com/blog)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.